package net.tepex.alarm.bots;

//import static org.junit.Assert.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;

import java.text.DecimalFormat;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

public class LocationTest
{
	@Test
	public void testBaseMsk()
	{
		assertThat(new Float(Location.GRADUS), is(0.01745329251994F));
		// BASE
		assertThat(new Float(Location.BASE_LAT), is(0.00000899321605919F));
		// MSK
		double radMsk = Location.CENTER_LATITUDE * Location.GRADUS;
		double cosRadMsk = Math.cos(radMsk);
		double msk = 1 / cosRadMsk;
		log.debug("msk: "+msk);
		log.debug("1 / cos(msk) = "+(1 / Math.cos(Location.CENTER_LATITUDE * Location.GRADUS)));
		assertThat(new Float(msk), is(1.7769114154238F));
			
		double baseMsk = Location.getBaseLat(Location.CENTER_LATITUDE);
		log.debug("baseMsk = "+baseMsk);
		assertThat(new Float(baseMsk), is(0.0000159801482772F));
	}
	
	@Test
	public void testLongitudeDistance()
	{
		double ln = Location.convertMskLongitudeDistance(300000);
		log.debug("ln: "+DF.format(ln));
		long lnInt = Math.round(ln*10);
		assertThat(lnInt, is(48L));
	}
	
	@Test
	public void testLatitudeDistance()
	{
		double lt = Location.convertLatitudeDistance(300000);
		log.debug("lt: "+DF.format(lt));
		long ltInt = Math.round(lt*10);
		assertThat(ltInt, is(27L));
	}
	
	private static final Logger log = getLogger(LocationTest.class);
	public static final DecimalFormat DF = new DecimalFormat("##.######");
}
