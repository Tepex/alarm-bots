package net.tepex.alarm.bots;

import java.text.DecimalFormat;

import java.util.Random;
import java.util.List;

import net.tepex.alarm.commons.resources.User;

import net.tepex.alarm.commons.PingAnswer;
import net.tepex.alarm.commons.Rest;
import net.tepex.alarm.commons.RestApi;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

public class Bot extends Thread
{
	public Bot(User user)
	{
		super(user.getLogin());
		this.user = user;
		//setDaemon(true);
	}
	
	@Override
	public void run()
	{
		log.info("Start bot \""+user+"\"");
		running = true;
		
		RestApi api = Rest.getApi(Rest.MAIN);
		while(running)
		{
			try
			{
				PingAnswer pa = ping(api);
				delay = pa.getPing()*1000;
				//System.out.println(pa);
			}
			catch(Exception e)
			{
				log.error("Server conection error.");
			}
			
			try
			{
				sleep(delay);
			}
			catch(InterruptedException e)
			{
				log.info("Bot \""+user+"\" interrupted");
				break;
			}
		}
	}
	
	public void shouldStop()
	{
		log.info("Stopping bot \""+user+"\"");
		running = false;
		interrupt();
		try
		{
			join();
		}
		catch(InterruptedException e)
		{
		}
	}
	
	private PingAnswer ping(RestApi api) throws Exception
	{
		// определяем случайный азимут
		int angle = rand.nextInt(360);
		double angleRad = (Math.PI*angle) / 180;
		// Перемещаемся случайно от 50 до 1000 м
		int distance = rand.nextInt(950)+50;
			
		int dx = (int)Math.round(Math.sin(angleRad) * distance);
		int dy = (int)Math.round(Math.cos(angleRad) * distance);
		move(dx, dy);
			
		log.info("Bot \""+user.getName()+"\" moved to "+dx+":"+dy);
		return api.ping(user.getId(), 0L, user.getLatitude(), user.getLongitude());
	}
	
	/**
	 * Возвращает коэффициент градуса для данной широты
	 */
	public static double getBaseLat(double latitude)
	{
		return BASE_LAT / Math.cos(latitude*GRADUS);
	}
	
	/**
	 * Конвертирует расстояние в м в градусы для данной широты.
	 * @param lat широта на которой конвертируется расстояние.
	 * @param distance расстояние в м.
	 * @return расстояние в градусах.
	 */
	public static double convertLongitudeDistance(double lat, int distance)
	{
		return getBaseLat(lat) * distance;
	}
	
	public static double convertLatitudeDistance(int distance)
	{
		return BASE_LAT * distance;
	}
	
	public static double convertMskLongitudeDistance(int distance)
	{
		return convertLongitudeDistance(CENTER_LATITUDE, distance);
	}
	
	/**
	 * Перемещение на заданное расстояние. Проверка выхода за границу периметра.
	 * @param dx расстояние по долготе в м
	 * @param dy расстояние по широте в м
	 */
	public void move(int dx, int dy)
	{
		double newLatitude = user.getLatitude() + convertLatitudeDistance(dy);
		double newLongitude = user.getLongitude() + convertLongitudeDistance(user.getLatitude(), dx);
		
		/* Если выявлен выход за периметр, то разворачиваем оглобли */
		if(newLatitude > MAX_LATITUDE || newLatitude < MIN_LATITUDE) newLatitude = user.getLatitude() - convertLatitudeDistance(dy);
		if(newLongitude > MAX_LONGITUDE || newLongitude < MIN_LONGITUDE) newLongitude = user.getLongitude() - convertLongitudeDistance(user.getLatitude(), dx);
		
		user.setLatitude(newLatitude);
		user.setLongitude(newLongitude);
	}

	private User user;
	private int delay;
	private volatile boolean running;
	private Random rand = new Random();
	private static final Logger log = getLogger(Bot.class);
	
	public static final double CENTER_LATITUDE = 55.752126;
	public static final double CENTER_LONGITUDE = 37.617294;
	
	public static final double MIN_LONGITUDE = 37.360519;
	public static final double MAX_LONGITUDE = 37.856282;
	public static final double MIN_LATITUDE = 55.566639;
	public static final double MAX_LATITUDE = 55.919892;
	
	public static final double BASE_LAT = 180 / (6371000 * Math.PI);
	public static final double GRADUS = Math.PI / 180;
}