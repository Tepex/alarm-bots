package net.tepex.alarm.bots;

import java.io.IOException;
import java.util.Locale;

import net.tepex.alarm.commons.resources.User;

import net.tepex.alarm.commons.Rest;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

public class Main
{
	public static final void main(String[] args)
	{
		Locale.setDefault(Locale.US);
		if(args.length > 1 && args[0].equals(ARG_SERVER)) SERVER_ADDRESS = args[1];
		
		try
		{
			System.in.close();
			System.out.close();
		}
		catch(IOException e)
		{
			log.error("Daemonize error!", e);
		}
		
		registerShutdownHook(Thread.currentThread());
		
		log.info("Start bots. Server: "+Rest.MAIN);
		
		User[] users = null;
		try
		{
			users = Rest.getApi(Rest.MAIN).users(0L, 0L, false, 2);
		}
		catch(Exception e)
		{
			log.error("getBots error!", e);
			return;
		}
		if(users == null || users.length == 0)
		{
			log.error("Error to get bot list");
			return;
		}
		
		log.info("Starting bots...");
		
		Bot[] bots = new Bot[users.length];
		for(int i = 0; i < users.length; ++i)
		{
			bots[i] = new Bot(users[i]);
			bots[i].start();
		}
		
		while(!shutdownFlag)
		{
			try
			{
				Thread.currentThread().join();
			}
			catch(InterruptedException e) {}
		}
		log.info("Stopping bots...");
		for(Bot bot: bots)
		{
			bot.shouldStop();
		}
		log.info("Bye!");
	}
	
	private static void registerShutdownHook(final Thread mainThread)
	{
		Runtime.getRuntime().addShutdownHook(new Thread()
		{
			@Override
			public void run()
			{
				log.info("shutdown hook");
				shutdownFlag = true;
				mainThread.interrupt();
			}
		});
	}
	
	private static final Logger log = getLogger(Main.class);
	private static volatile boolean shutdownFlag = false;
	
	public static String SERVER_ADDRESS = "http://tepex.net:8080/alarm";
	
	public static final String ARG_SERVER = "-server";
	
	public static final String URL_LOGIN = "/login";
	public static final String URL_PING = "/ping";
	
	public static final String RESP_MSG = "msg";
}