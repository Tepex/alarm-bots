package net.tepex.alarm.bots;

import java.text.DecimalFormat;

public class Location
{
	public Location(double latitude, double longitude)
	{
		this.latitude = latitude;
		this.longitude = longitude;
		init();
	}
	
	private void init()
	{
		int lt = (int)(latitude * 1000000);
		int lg = (int)(longitude * 1000000);
		hashCode = lt*lg;
		updateToString();
	}
	
	public double getLatitude()
	{
		return latitude;
	}
	
	public double getLongitude()
	{
		return longitude;
	}
	
	@Override
	public int hashCode()
	{
		return hashCode;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null) return false;
		if(!(obj instanceof Location)) return false;
		Location other = (Location)obj;
		return (other.latitude == latitude && other.longitude == longitude);
	}
	
	@Override
	public String toString()
	{
		return toString;
	}
	
	private void updateToString()
	{
		toString = DF.format(latitude)+":"+DF.format(longitude);
	}
	
	/**
	 * Возвращает коэффициент градуса для данной широты
	 */
	public static double getBaseLat(double latitude)
	{
		return BASE_LAT / Math.cos(latitude*GRADUS);
	}
	
	/**
	 * Конвертирует расстояние в м в градусы для данной широты.
	 * @param lat широта на которой конвертируется расстояние.
	 * @param distance расстояние в м.
	 * @return расстояние в градусах.
	 */
	public static double convertLongitudeDistance(double lat, int distance)
	{
		return getBaseLat(lat) * distance;
	}
	
	public static double convertLatitudeDistance(int distance)
	{
		return BASE_LAT * distance;
	}
	
	public static double convertMskLongitudeDistance(int distance)
	{
		return convertLongitudeDistance(CENTER_LATITUDE, distance);
	}
	
	/**
	 * Перемещение на заданное расстояние. Проверка выхода за границу периметра.
	 * @param dx расстояние по долготе в м
	 * @param dy расстояние по широте в м
	 */
	public void move(int dx, int dy)
	{
		double newLatitude = latitude + Location.convertLatitudeDistance(dy);
		double newLongitude = longitude + Location.convertLongitudeDistance(latitude, dx);
		
		/* Если выявлен выход за периметр, то разворачиваем оглобли */
		if(newLatitude > MAX_LATITUDE || newLatitude < MIN_LATITUDE) newLatitude = latitude - Location.convertLatitudeDistance(dy);
		if(newLongitude > MAX_LONGITUDE || newLongitude < MIN_LONGITUDE) newLongitude = longitude - Location.convertLongitudeDistance(latitude, dx);
		
		latitude = newLatitude;
		longitude = newLongitude;
		updateToString();
	}
	
	private double latitude;
	private double longitude;
	private int hashCode;
	private String toString;
	
	public final DecimalFormat DF = new DecimalFormat("##.######");;
	public static final double CENTER_LATITUDE = 55.752126;
	public static final double CENTER_LONGITUDE = 37.617294;
	
	public static final double MIN_LONGITUDE = 37.360519;
	public static final double MAX_LONGITUDE = 37.856282;
	public static final double MIN_LATITUDE = 55.566639;
	public static final double MAX_LATITUDE = 55.919892;
	
	public static final double BASE_LAT = 180 / (6371000 * Math.PI);
	public static final double GRADUS = Math.PI / 180;
	
	public static final String PARAM_LATITUDE = "latitude";
	public static final String PARAM_LONGITUDE = "longitude";
}
